/* Cupla formula super driver bot */
/* copyright (c) 2014 Teemu L�tti, Toni Tammelander */

package noobbot;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import noobbot.GameInitMsg.Data.Race.Track.Piece;
import noobbot.Main.State;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/** The Main class */
public class Main {

    /*
     * MAKE COMMITS WITH: DEBUG = 0 TRACK = 0 CARS = 1 !!!
     */

    /**
     * Debug level: 0==only some, no file / 1==detailed and file
     */
    public static final int DEBUG = 0;

    /** Which track to race: 0 == default (finland), 1 == germany, 2 == usa, 3 == france */
    public static final int TRACK = 0;
    public static final int CARS = 1;

    /** The main method */
    public static void main( String... args ) throws IOException {
        // Parameters
        String host = args[0];
        int port = Integer.parseInt( args[1] );
        String botName = args[2];
        String botKey = args[3];
        String race = ( args.length >= 5 ) ? args[4] : "";

        // Create log file
        if ( DEBUG >= 1 ) {
            logfile = new PrintStream( new File( "log.txt" ) );
        }

        // Run the game
        log0( "connecting to " + host + ":" + port + " as " + botName + "/" + botKey );
        final Main main = new Main();
        try {
            main.run( new Socket( host, port ), botName, botKey, race );
        } catch ( Exception e ) {
            e.printStackTrace();
        }

        // Exit
        log0( "exit" );
        if ( logfile != null ) {
            logfile.close();
        }
    }

    private BufferedReader reader;
    private PrintWriter writer;
    private final Gson gson = new Gson();
    private static PrintStream logfile;

    /** Current state of the race */
    private final State state = new State();

    /** Race state class */
    public static class State {
        /** Current tick in the race */
        public int tick;

        /** Number of cars in the race */
        public int cars = 1;

        /** Lanes on the track */
        public GameInitMsg.Data.Race.Track.Lane[] lanes;

        /** Turbo available now? */
        public boolean turboAvailable;

        /** Actual race (instead of practice/qualification)? */
        public boolean actualRace = false;

        /** Time when currently processed message was received (ms) */
        public long time;

        /** Delta time from message receiving (ms) */
        public long time() {
            return System.currentTimeMillis() - time;
        }

        /** Initializes race state */
        public void init() {
            tick = -1;
            turboAvailable = false;
        }
    }

    /** Current state of the car */
    private final CarState car = new CarState();

    /** Car state class */
    public static class CarState {

        /** Car color */
        private String color = "";

        /** Distance within current piece */
        public double inPieceDistance;

        /** Piece where car is */
        public GameInitMsg.Data.Race.Track.Piece piece;

        /** Piece where car was before this tick was updated */
        public GameInitMsg.Data.Race.Track.Piece oldPiece;

        /** Current lane index */
        public int lane;

        /** Car throttle (as last time we sent it) */
        public double throttle;

        /** Car speed [distance_units / tick] */
        public double speed;

        /** Car drift angle (the real value signed) */
        public double angleSign;

        /** Car drift angle (absolute unsigned) */
        public double angle;

        /** Car drift angle change since last time (delta is signed but taken from unsigned angle) */
        public double angleDelta;

        /** Car is currently crashed? */
        public boolean isCrashed;

        /** Lane switch message already sent during this piece? */
        public boolean switchLaneSent;

        /** Wrong lane when trying to pass, so do not apply algorithm now */
        public boolean wrongLane;

        /** Closest opponent (just in front of us) or null */
        public CarPositionsMsg.Data opponent;

        /** Initializes car state */
        public void init() {
            piece = null;
            oldPiece = null;
            lane = 0;
            throttle = 0;
            speed = 0;
            angle = 0;
            angleDelta = 0;
            isCrashed = false;
            switchLaneSent = false;
            wrongLane = false;
            opponent = null;
        }
    }

    /** List of pieces that make up this track */
    private List<GameInitMsg.Data.Race.Track.Piece> pieces = null;

    /** Main constructor */
    public Main() {
    }

    /** Runs the race */
    public void run( Socket socket, String botName, String botKey, String race ) throws IOException {
        // Socket reader and writer
        reader = new BufferedReader( new InputStreamReader( socket.getInputStream(), "utf-8" ) );
        writer = new PrintWriter( new OutputStreamWriter( socket.getOutputStream(), "utf-8" ) );

        // Join race/track
        log0( "joining" );
        if ( TRACK == 0 ) {
            send( new JoinMsg( botName, botKey ) );
            log0( "keimola" );
        } else if ( TRACK == 1 ) {
            if ( race.equals( "create" ) ) {
                CreateRaceMsg rmsg = new CreateRaceMsg( botName, botKey, "germany", CARS );
                rmsg.password = "bumblebee";
                send( rmsg );
            } else if ( race.equals( "join" ) ) {
                JoinRaceMsg rmsg = new JoinRaceMsg( botName, botKey, "germany", CARS );
                rmsg.password = "bumblebee";
                send( rmsg );
            } else {
                send( new JoinRaceMsg( botName, botKey, "germany", CARS ) );
            }
            log0( "germany" );
        } else if ( TRACK == 2 ) {
            send( new JoinRaceMsg( botName, botKey, "usa", CARS ) );
            log0( "usa" );
        } else if ( TRACK == 3 ) {
            send( new JoinRaceMsg( botName, botKey, "france", CARS ) );
            log0( "france" );
        }

        // Server socket receiver loop
        String line = null;
        while ( ( line = reader.readLine() ) != null ) {
            state.time = System.currentTimeMillis();
            final MsgWrapper msgFromServer = gson.fromJson( line, MsgWrapper.class );
            String type = msgFromServer.msgType;

            // Process received message
            if ( type.equals( "carPositions" ) ) {
                // log( "carPositions" );
                final CarPositionsMsg msg = gson.fromJson( line, CarPositionsMsg.class );

                // Update based on received information
                updateEverything( msg );

                // Decide throttle and send it
                car.throttle = getThrottle();
                send( new ThrottleMsg( car.throttle, state.tick ) );

                // Update passed piece
                updateEverythingPost();

                // Log time used to process
                long time = state.time();
                if ( time >= 30 ) {
                    log0( "tick=" + state.tick + " time=" + time );
                }
            } else if ( type.equals( "join" ) || type.equals( "joinRace" ) || type.equals( "createRace" ) ) {
                log0( "joined" );
            } else if ( type.equals( "yourCar" ) ) {
                // My car information
                final YourCarMsg msg = gson.fromJson( line, YourCarMsg.class );
                car.color = msg.data.color;
                log0( "yourCar: color=" + car.color );
            } else if ( type.equals( "gameInit" ) ) {
                // Qualification/race initialization
                log0( "gameInit" );
                final GameInitMsg msg = gson.fromJson( line, GameInitMsg.class );
                state.init();
                car.init();
                initEverything( msg );
            } else if ( type.equals( "gameEnd" ) ) {
                log0( "gameEnd" );
            } else if ( type.equals( "gameStart" ) ) {
                log0( "gameStart" );
                send( new PingMsg() ); // seems to be required
            } else if ( type.equals( "tournamentEnd" ) ) {
                log0( "tournamentEnd" );
            } else if ( type.equals( "crash" ) ) {
                // Somebody crashed (not necessarily us)
                final CrashMsg msg = gson.fromJson( line, CrashMsg.class );
                if ( msg.data.color.equals( car.color ) ) {
                    // We crashed
                    log0( "crash: angle=" + Utils.toStr( car.angleSign, 0, true ) );
                    car.isCrashed = true;
                    car.piece.maxDriftAngle = 99; // means crash
                }
            } else if ( type.equals( "spawn" ) ) {
                // Somebody was spawn back (not necessarily us)
                final SpawnMsg msg = gson.fromJson( line, SpawnMsg.class );
                if ( msg.data.color.equals( car.color ) ) {
                    // We spawn
                    log0( "spawn" );
                    car.isCrashed = false;
                }
            } else if ( type.equals( "lapFinished" ) ) {
                // Lap finished
                final LapFinishedMsg msg = gson.fromJson( line, LapFinishedMsg.class );
                log0( "lapFinished (" + msg.data.lapTime.lap + "): " + Utils.toStr( msg.data.lapTime.millis / 1000.0, 2, false ) + "s" );
            } else if ( type.equals( "turboAvailable" ) ) {
                // Turbo available
                log0( "turboAvailable" );
                state.turboAvailable = true;
            } else if ( type.equals( "turboStart" ) ) {
                log0( "turboStart" );
            } else if ( type.equals( "turboEnd" ) ) {
                log0( "turboEnd" );
            } else if ( type.equals( "dnf" ) ) {
                log0( "dnf" );
            } else if ( type.equals( "finish" ) ) {
                log0( "finish" );
            } else if ( type.equals( "error" ) ) {
                log0( "error:\r\n" + line );
            } else {
                log0( "unknown message: " + type + "\r\n" + line );
                send( new PingMsg() );
            }
        }

        // Done
        reader = null;
        writer = null;
        socket.close();
    }

    public static class CurveInfo {
        public double angleDistance;
        public double distance;
        public Piece nextPiece;
    }

    private double calcTargetSpeedForCurve( double piece_radius, double lane_dist_from_center, double curve_angle_remaining ) {
        // a = (v**2)/radius
        // it seems acceleration a is about 0.5, because
        // 6.5**2/(100-20) --> car barely stays on track

        // HINT! max_centric_acc affects the curve speed. Lower the value
        // to get lower speeds.
        // double max_centric_acc = 0.528; // Suomi and Germany
        double max_centric_acc = 0.5; // USA
        double radius = piece_radius + lane_dist_from_center;

        double radius_mod = Utils.mapvalue( radius, 50, 100, 0.95, 1.0 );
        // curve with small radius seems to be harder
        // calculate square root of acceleration * radius
        double max_speed_in_long_curve = Math.sqrt( max_centric_acc * radius * radius_mod );
        if ( false ) {
            return max_speed_in_long_curve;
        } else {
            // long_curve is >= 90 degrees
            double max_speed_in_low_curve = max_speed_in_long_curve * 1.35;
            double max_speed = Utils.mapvalue( curve_angle_remaining, 22, 90, max_speed_in_low_curve, max_speed_in_long_curve );
            return max_speed;
        }
    }

    private CurveInfo curveDistance( Piece piece, double in_piece_dist ) {
        // piece - piece where to start calculating distance
        // in_piece_dist - offset where to start inside piece
        double initial_angle = piece.angle;
        double initial_radius = piece.radius;
        double modifier = 1.0;

        double piece_len = ( 2 * Math.PI * piece.radius * Math.abs( piece.angle ) ) / 360.0;
        double distance = piece_len - in_piece_dist;
        double angle_distance = ( Math.abs( initial_angle ) * distance ) / piece.length( state, car.lane );
        piece = piece.next;

        while ( piece.isCurve() && ( ( ( initial_angle > 0 ) && ( piece.angle > 0 ) ) || ( ( initial_angle < 0 ) && ( piece.angle < 0 ) ) ) ) {
            if ( initial_radius != piece.radius ) {
                // This curve piece has different radius
                // HINT! Here we have two options:
                // a) handle this curve piece as part of the entire long curve
                // b) handle this curve piece as separate curve
                // We choose b and exit the while loop
                break;
                // for example rad 50 changes to 100 rad --> modifier 0.5
                // modifier = piece.radius / initial_radius;
            }
            piece_len = ( 2 * Math.PI * piece.radius * Math.abs( piece.angle ) ) / 360.0;
            distance += piece_len;
            angle_distance += Math.abs( piece.angle ) * modifier;
            piece = piece.next;
        }

        // distance is the distance car would travel
        // piece is the next piece after this curve
        // angle_distance is angle car would travel

        CurveInfo curveInfo = new CurveInfo();
        curveInfo.distance = distance;
        curveInfo.angleDistance = angle_distance;
        curveInfo.nextPiece = piece;
        return curveInfo;
    }

    /** Initializes everything in beginning of race */
    private void initEverything( GameInitMsg msg ) {

        // Init only once
        if ( pieces == null ) {
            log0( "initializing pieces" );
            pieces = new ArrayList<GameInitMsg.Data.Race.Track.Piece>();

            // Collect pieces
            Piece prev = msg.data.race.track.pieces[msg.data.race.track.pieces.length - 1];
            for ( int i = 0; i < msg.data.race.track.pieces.length; i++ ) {
                Piece piece = msg.data.race.track.pieces[i];
                piece.index = i;
                piece.prev = prev;
                prev.next = piece;
                log( "piece: "
                        + piece.index
                        + " length="
                        + Utils.toStr( piece.length( state, -1 ), 1, false )
                        + ( ( piece.angle != 0 ) ? ( " angle=" + Utils.toStr( piece.angle, 0, true ) + " radius=" + Utils.toStr( piece.radius,
                                                                                                                                 1,
                                                                                                                                 false ) )
                                : "" ) );
                pieces.add( piece );
                prev = piece;
            }

            // Lanes
            state.lanes = msg.data.race.track.lanes;

            // Initialize speed limits to pieces
            double laneDist = 0;
            for ( GameInitMsg.Data.Race.Track.Lane lane : state.lanes ) {
                if ( lane.distanceFromCenter < laneDist ) {
                    laneDist = lane.distanceFromCenter; // most inner lane
                }
            }
            Piece piece = pieces.get( 0 );
            Piece lastPiece;
            CurveInfo curveInfo;
            int straights = 0;
            while ( true ) {
                lastPiece = piece;
                if ( piece.isStraight() ) {
                    piece.speedLimit = 100.0;
                    piece = piece.next;
                    straights++;
                } else {
                    curveInfo = curveDistance( piece, 0 );
                    // calculate optimal speed for entire curve
                    double targetSpeed = calcTargetSpeedForCurve( piece.radius, laneDist, curveInfo.angleDistance );
                    // loop all pieces in curve and set speed limit
                    Piece tempPiece = lastPiece;
                    while ( tempPiece.index < curveInfo.nextPiece.index ) {
                        tempPiece.speedLimit = targetSpeed * 0.95;
                        tempPiece = tempPiece.next;
                    }
                    if ( straights >= 2 ) {
                        // First piece in the curve after some straight can go faster
                        lastPiece.speedLimit = targetSpeed * 1.02;
                    }
                    if ( piece.radius < piece.prev.radius ) {
                        piece.speedLimit *= 0.95;
                    }
                    piece = curveInfo.nextPiece;
                    straights = 0;
                }
                if ( piece.index < lastPiece.index ) {
                    // we have looped entire track
                    break;
                }
            }
        } else {
            // Pieces were already initialized, must be actual race
            log0( "actual race" );
            state.actualRace = true;
        }
        log0( "pieces count=" + pieces.size() );
    }

    /** Updates everything based on received message */
    private void updateEverything( CarPositionsMsg msg ) {

        // Current tick
        int oldTick = state.tick;
        state.tick = msg.gameTick;

        // Number of cars
        state.cars = msg.data.length;

        // !timing
        if ( state.time() >= 40 ) {
            log0( "!time=" + state.time() );
        }

        // Find my car data (along other cars)
        CarPositionsMsg.Data carData = null;
        for ( CarPositionsMsg.Data d : msg.data ) {
            if ( d.id.color.equals( car.color ) ) {
                // My car data
                carData = d;
            }
        }

        // !timing
        if ( state.time() >= 40 ) {
            log0( "!time=" + state.time() );
        }

        // Find closest opponent
        car.opponent = null;
        for ( CarPositionsMsg.Data d : msg.data ) {
            if ( d != carData ) {
                if ( ( ( d.piecePosition.pieceIndex == carData.piecePosition.pieceIndex ) && ( d.piecePosition.inPieceDistance > carData.piecePosition.inPieceDistance ) )
                        || ( d.piecePosition.pieceIndex == ( carData.piecePosition.pieceIndex + 1 ) )
                        || ( d.piecePosition.pieceIndex == ( carData.piecePosition.pieceIndex + 2 ) ) ) {
                    // This car is just in front of us
                    car.opponent = d;
                }
            }
        }

        // !timing
        if ( state.time() >= 40 ) {
            log0( "!time=" + state.time() );
        }

        // Update car state
        car.oldPiece = car.piece;
        car.piece = pieces.get( carData.piecePosition.pieceIndex );
        double oldCarDistance = car.inPieceDistance;
        if ( ( car.piece != car.oldPiece ) && ( car.oldPiece != null ) ) {
            // Piece changed, so old inPieceDistance will be negative relative to new current piece
            oldCarDistance -= car.oldPiece.length( state, car.lane );
        }
        car.inPieceDistance = carData.piecePosition.inPieceDistance;
        car.lane = carData.piecePosition.lane.endLaneIndex;
        double oldCarAngle = car.angle;
        car.angleSign = carData.angle; // signed
        car.angle = Math.abs( car.angleSign ); // unsigned
        car.angleDelta = car.angle - oldCarAngle; // delta from unsigned
        if ( ( car.inPieceDistance > 0 ) && ( oldTick >= 0 ) && ( ( state.tick - oldTick ) > 0 ) ) {
            // Car speed is change of distance per ticks
            car.speed = ( car.inPieceDistance - oldCarDistance ) / ( state.tick - oldTick );
        } else {
            // No data yet, so speed zero
            car.speed = 0;
        }

        // !timing
        if ( state.time() >= 40 ) {
            log0( "!time=" + state.time() );
        }

        // Reset things when piece changed
        if ( car.piece != car.oldPiece ) {
            car.piece.maxDriftAngle = 0;
            car.piece.speedMax = 0;
            car.switchLaneSent = false;
        }

        // Update piece max drift angle
        if ( car.angle > car.piece.maxDriftAngle ) {
            car.piece.maxDriftAngle = car.angle;
        }

        // Update car max speed within this piece
        if ( car.speed > car.piece.speedMax ) {
            car.piece.speedMax = car.speed;
        }

        // Log new piece
        if ( car.piece != car.oldPiece ) {
            if ( car.piece.index == 0 ) {
                log( "" );
            }
            log( "piece: "
                    + car.piece.index
                    + ( ( car.piece.angle != 0 ) ? ( " angle=" + Utils.toStr( car.piece.angle, 0, true ) + " radius=" + Utils.toStr( car.piece.radius,
                                                                                                                                     1,
                                                                                                                                     false ) )
                            : " straight" ) + " speedLimit=" + Utils.toStr( car.piece.speedLimit, 1, false ) );
        }
    }

    /** Updates piece that was passed (as the last thing in tick) */
    private void updateEverythingPost() {
        // Piece changed and there is old piece (null when started)
        if ( ( car.piece != car.oldPiece ) && ( car.oldPiece != null ) ) {

            // Make sure speed max has some value (used below)
            if ( car.oldPiece.speedMax <= 0 ) {
                car.oldPiece.speedMax = car.oldPiece.speedLimit;
            }

            // Count how many curves in row backwards
            int curvesInRow = 0;
            GameInitMsg.Data.Race.Track.Piece pp = car.oldPiece;
            while ( pp.isCurve() ) {
                curvesInRow++;
                pp = pp.prev;
            }
            if ( curvesInRow == 0 ) {
                curvesInRow = 1; // on straight, do not divide with zero
            }

            // Adjust speed limit for passed piece
            double oldSpeedLimit = car.oldPiece.speedLimit;
            String logmsg = "stay at ";
            if ( car.oldPiece.maxDriftAngle == 99 ) {
                // Crash => slow down a lot from current SPEED
                car.oldPiece.speedLimit = car.oldPiece.speedMax - 1.0;
                logmsg = "-1.0 to ";
            } else if ( car.oldPiece.maxDriftAngle >= 58 ) {
                // Too much => slow down from current SPEED
                car.oldPiece.speedLimit = car.oldPiece.speedMax - 0.1;
                car.oldPiece.prev.speedLimit -= 0.1; // previous piece
                car.oldPiece.prev.prev.speedLimit -= 0.1; // previous previous piece
                logmsg = "-0.1 to ";
            } else if ( car.oldPiece.maxDriftAngle >= 57 ) {
                // Too much => slow down from current SPEED
                car.oldPiece.speedLimit = car.oldPiece.speedMax - 0.05;
                logmsg = "-0.05 to ";
            } else if ( ( car.oldPiece.maxDriftAngle >= 50 ) && ( car.oldPiece.speedMax < car.oldPiece.speedLimit ) ) {
                // Quite much drift and the limit is higher than current speed => limit here
                car.oldPiece.speedLimit = car.oldPiece.speedMax;
                logmsg = "to ";
            } else if ( !state.actualRace && !car.wrongLane ) {
                // Dare not speed up in actual race! AND Do not speed up when going in wrong lane!
                if ( car.oldPiece.maxDriftAngle < 20 ) {
                    // Speed up from current SPEED because very much space
                    if ( car.oldPiece.speedMax >= car.oldPiece.speedLimit ) {
                        car.oldPiece.speedLimit = car.oldPiece.speedMax + ( 0.1 / curvesInRow );
                        logmsg = "+0.1 to ";
                    }
                } else if ( car.oldPiece.maxDriftAngle < 40 ) {
                    // Speed up from current SPEED because very much space
                    if ( car.oldPiece.speedMax >= car.oldPiece.speedLimit ) {
                        car.oldPiece.speedLimit = car.oldPiece.speedMax + ( 0.1 / curvesInRow );
                        logmsg = "+0.1 to ";
                    }
                } else if ( car.oldPiece.maxDriftAngle < 54 ) {
                    // Speed up from current LIMIT because close
                    if ( car.oldPiece.speedMax >= car.oldPiece.speedLimit ) {
                        car.oldPiece.speedLimit += ( 0.1 / curvesInRow );
                        logmsg = "+0.1 to ";
                    }
                }
            }

            // Limit the speed limit to reasonable value
            if ( car.oldPiece.speedLimit < 1.0 ) {
                car.oldPiece.speedLimit = 1.0;
            }

            // Log speed limit change if any
            if ( car.oldPiece.speedLimit != oldSpeedLimit ) {
                log( "    piece " + car.oldPiece.index + " speedLimit " + logmsg + Utils.toStr( car.oldPiece.speedLimit, 1, false ) );
            }
        }
    }

    private double calcBrakingDist( double curr_speed, double target_speed ) {
        // Call this method on straight road to calculate distance that is
        // required to slow down (brake) before a curve, where we need to
        // have target_speed. curr_speed is current car speed.
        // !!! 3 x current_speed TO ADD TO THIS!
        final double neperc = 2.7182;
        final double constant = -0.02;
        double brake_time;
        double integral_1;
        double integral_2;
        double distance;
        if ( curr_speed == 0 ) {
            return 0;
        }
        if ( target_speed == 0 ) {
            target_speed = 0.001;
        }

        brake_time = -Math.log( curr_speed / target_speed ) / constant;
        integral_1 = curr_speed * ( 1 / constant ) * Math.pow( Math.E, ( constant * brake_time ) );
        integral_2 = curr_speed * ( 1 / constant ) * Math.pow( Math.E, ( constant * 0 ) );
        distance = integral_1 - integral_2;
        return distance;
    }

    /** Gets throttle for sending to server. Also may send other messages before that. */
    private double getThrottle() {

        // Emergency timing
        if ( state.time() >= 40 ) {
            log0( "time emergency 1" );
            return 0.4;
        }

        // The algorithm
        double throttle = getThrottle_Adapter();

        // Emergency timing
        if ( state.time() >= 40 ) {
            log0( "time emergency 2" );
            return 0.4;
        }

        // Switch lane when makes sense. NOTE also that server ignores all but first message, so switching lane make the
        // throttle message to be ignored (should not matter too much in straight pieces anyway).
        laneSwitcher();

        // Emergency timing
        if ( state.time() >= 40 ) {
            log0( "time emergency 3" );
            return 0.4;
        }

        // Use turbo if available and makes sense right now (also eats throttle message)
        turboHandler();

        // Log
        if ( !car.isCrashed ) {
            log( "    " + Utils.toStr( car.speed, 1, false ) + "  " + Utils.toStr( car.angleSign, 0, true ) + "  "
                    + Utils.toStr( throttle, 2, false ) + "  " + Utils.toStr( car.inPieceDistance, 1, false ) + " / "
                    + Utils.toStr( car.piece.length( state, car.lane ), 1, false ) );
        }

        // Done
        /*
         * // Server says sometimes invalid throttle value. What is wrong? if (throttle > 1.0) { throttle = 1.0; } else
         * if (throttle < 0.0) { throttle = 0.0; }
         */
        return throttle;
    }

    /** Algorithm: adapt piece speed limit according to drift */
    private double getThrottle_Adapter() {

        // Speed limit for this piece
        double targetSpeed = car.piece.speedLimit;

        // Speed limit for upcoming pieces so that we will not exit this piece with too much speed (but increasingly
        // closer to the end of this piece)
        double targetSpeedNext = 999;
        double distance = car.piece.length( state, car.lane ) - car.inPieceDistance; // distance left in this piece
        GameInitMsg.Data.Race.Track.Piece p = car.piece.next;
        for ( int i = 0; i < 10; i++ ) {
            // Speed target for upcoming piece going up further back we go
            // ~70 distance_units seems to break for 1.0 speed_unit
            double targetSpeedNext2 = p.speedLimit + ( distance / 70.0 );
            if ( targetSpeedNext2 < targetSpeedNext ) {
                // But was lower than what we had => use it
                targetSpeedNext = targetSpeedNext2;
            }
            distance += p.length( state, car.lane ); // upcoming piece distance
            p = p.next;
        }

        // Use upcoming pieces' speed limit if it is lower than ours
        if ( targetSpeedNext < targetSpeed ) {
            targetSpeed = targetSpeedNext;
        }

        // Instead of controlling throttle precisely, accelerate/deaccelerate with full throttle, because it was shown
        // by Toni that speed changes faster this way.
        double throttle;
        if ( (int)( car.speed * 10 ) == (int)( targetSpeed * 10 ) ) {
            // Keep
            throttle = targetSpeed / 10;
            if ( car.piece.speedMax == car.speed ) {
                car.piece.speedMax = targetSpeed;
            }
        } else if ( car.speed > targetSpeed ) {
            // Break
            throttle = 0.0;
        } else {
            // Accelerate
            throttle = 1.0;
        }

        // Emergency break when drift raises too fast
        if ( ( car.angle >= 45 ) && ( car.angleDelta >= 4 ) ) {
            throttle = 0.0;
        }

        // Emergency break when drifting too much
        if ( car.angle >= 57 ) {
            throttle = 0.0;
        }

        // Make sure we never provide invalid values to throttle
        if ( throttle < 0.0 ) {
            throttle = 0.0;
        } else if ( throttle > 1.0 ) {
            throttle = 1.0;
        }

        // Done
        return throttle;
    }

    /** Switches lane when makes sense */
    private void laneSwitcher() {

        // Only send once per piece
        if ( !car.switchLaneSent ) {
            // Can we switch lane in next piece? (used to be this piece, but it seems that changing lane does not work
            // if it is not done early enough, so we send the message one piece before, which seems to work)
            Piece piece = car.piece.next;
            if ( piece.switchLane ) {
                // Find next switch piece and calculate lane lengths until that
                int laneLeftIndex = ( car.lane > 0 ) ? ( car.lane - 1 ) : -1;
                int laneRightIndex = ( car.lane < ( state.lanes.length - 1 ) ) ? ( car.lane + 1 ) : -1;
                double laneLeftLength = 0;
                double laneThisLength = 0;
                double laneRightLength = 0;
                piece = piece.next;
                while ( !piece.switchLane ) {
                    if ( laneLeftIndex > -1 ) {
                        laneLeftLength += piece.length( state, laneLeftIndex );
                    }
                    laneThisLength += piece.length( state, car.lane );
                    if ( laneRightIndex > -1 ) {
                        laneRightLength += piece.length( state, laneRightIndex );
                    }
                    piece = piece.next;
                }

                // Only switch lane if makes any difference
                boolean switchLeft = ( ( laneLeftLength > 0 ) && ( laneLeftLength < laneThisLength ) );
                boolean switchRight = ( ( laneRightLength > 0 ) && ( laneRightLength < laneThisLength ) );
                if ( switchLeft || switchRight ) {
                    // Switch to shorter lane
                    if ( switchLeft && switchRight && ( laneLeftLength < laneRightLength ) ) {
                        switchRight = false;
                    }

                    // Send message to switch lane
                    send( new SwitchLaneMsg( switchRight ) );
                    log( "    switchLane: " + ( ( switchRight ) ? "right" : "left" ) );

                    // Message sent now for this piece
                    car.switchLaneSent = true;
                    car.wrongLane = false;
                } else {
                    // Does not make difference => check opponent
                    if ( ( car.opponent != null ) && ( car.opponent.piecePosition.lane.endLaneIndex == car.lane ) ) {
                        // Opponent in front of us in the same lane => switch lane to take over
                        boolean switchRight2 = ( car.lane < ( state.lanes.length - 1 ) );
                        send( new SwitchLaneMsg( switchRight2 ) );
                        car.switchLaneSent = true;
                        car.wrongLane = true;
                    } else {
                        car.wrongLane = false;
                    }
                }
            }
        }
    }

    /** Handles turbo usage */
    private void turboHandler() {
        boolean turbo = false;

        // Available?
        if ( state.turboAvailable ) {
            // Count how many straights there is ahead
            int straights = 0;
            GameInitMsg.Data.Race.Track.Piece piece = car.piece;
            while ( piece.isStraight() ) {
                straights++;
                piece = piece.next;
            }

            // Only if there is enough straight ahead because otherwise turbo will kill us
            if ( straights >= 5 ) {
                // Are we alone (racing against time)?
                if ( !state.actualRace ) {
                    // Qualification => yes
                    turbo = true;
                } else if ( car.opponent != null ) {
                    // Race and opponent in front of us => yes
                    turbo = true;
                }
            }
        }

        // Use turbo now?
        if ( turbo ) {
            send( new TurboMsg() ); // NOTE this eats the throttle message sent after this
            state.turboAvailable = false;
            log0( "turbo!" );
        }
    }

    /** Sends message to server */
    private void send( final SendMsg msg ) {
        writer.println( msg.toJson() );
        writer.flush();
    }

    private void send( final ThrottleMsg msg ) {
        writer.println( msg.toJson() );
        writer.flush();
    }

    private void send( final SwitchLaneMsg msg ) {
        writer.println( msg.toJson() );
        writer.flush();
    }

    private void send( final TurboMsg msg ) {
        writer.println( msg.toJson() );
        writer.flush();
    }

    /** Logs message only if debug level is set */
    public static void log( String str ) {
        if ( DEBUG >= 1 ) {
            log0( str );
        }
    }

    /** Logs message also at debug level 0 */
    public static void log0( String str ) {
        System.out.println( str );
        if ( logfile != null ) {
            logfile.println( str );
        }
    }
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper( final String msgType, final Object data ) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper( final SendMsg sendMsg ) {
        this( sendMsg.msgType(), sendMsg.msgData() );
    }
}

class YourCarMsg {
    public String msgType;
    public Data data;

    public static class Data {
        public String name;
        public String color;
    }
}

class CarPositionsMsg {
    public String msgType;
    public Data[] data;

    public static class Data {
        public Id id;

        public static class Id {
            public String name;
            public String color;
        }

        /** Car drifting angle */
        public double angle;

        public PiecePosition piecePosition;

        public static class PiecePosition {
            public int pieceIndex;
            public double inPieceDistance;
            public Lane lane;

            public static class Lane {
                public int startLaneIndex;
                public int endLaneIndex;
            }

            public int lap;
        }
    }

    public int gameTick;
}

class CrashMsg {
    public String msgType;
    public Data data;

    public static class Data {
        public String name;
        public String color;
    }
}

class SpawnMsg {
    public String msgType;
    public Data data;

    public static class Data {
        public String name;
        public String color;
    }
}

class LapFinishedMsg {
    public String msgType;
    public Data data;

    public static class Data {
        public LapTime lapTime;

        public static class LapTime {
            public int lap;
            public int ticks;

            /** Lap time in millis */
            public int millis;
        }
    }
}

class GameInitMsg {
    public String msgType;
    public Data data;

    public static class Data {
        public Race race;

        public static class Race {
            public Track track;

            public static class Track {
                public String id;
                public String name;
                public Piece[] pieces;

                public static class Piece {
                    private double length; // private so force to use length()
                    @SerializedName( "switch" )
                    public boolean switchLane;
                    public int radius;
                    public double angle;

                    /** Returns piece length that depends on the lane for curves. If laneIndex==-1 then does not adjust. */
                    public double length( State state, int laneIndex ) {
                        if ( this.angle != 0 ) {
                            // Curve => calculate using current lane
                            double r = this.radius;
                            if ( ( state.lanes != null ) && ( laneIndex >= 0 ) && ( laneIndex < state.lanes.length ) ) {
                                r += state.lanes[laneIndex].distanceFromCenter * ( this.angle > 0 ? -1 : +1 );
                            }
                            return ( 2 * Math.PI * r * Math.abs( this.angle ) ) / 360.0;
                        } else {
                            // Straight
                            return this.length;
                        }
                    }

                    /**
                     * Returns difficulty factor. This is zero for straight and bigger value means harder curve. Curve
                     * with radius 200 has value 1.0.
                     */
                    public double factor() {
                        if ( this.radius == 0 ) {
                            return 0;
                        } else {
                            return ( 200.0 / this.radius );
                        }
                    }

                    /** Index in the containing array "pieces" */
                    public int index;

                    /** Previous piece in the track */
                    public Piece prev;

                    /** Next piece in the track */
                    public Piece next;

                    /** Maximum drift angle we have got in this piece */
                    public double maxDriftAngle = -1;

                    /** Max speed we had in this track */
                    public double speedMax = 0;

                    /** Max speed allowed in this piece (algorithm) */
                    public double speedLimit = -1;

                    /** Returns if this piece is straight (instead of curve) */
                    public boolean isStraight() {
                        return ( angle == 0 );
                    }

                    /** Returns if this piece is curve (instead of straight) */
                    public boolean isCurve() {
                        return ( angle != 0 );
                    }
                }

                public Lane[] lanes;

                public static class Lane {
                    public double distanceFromCenter;
                    public int index;
                }
            }
        }
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson( new MsgWrapper( this ) );
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class JoinMsg extends SendMsg {
    public final String name;
    public final String key;

    JoinMsg( final String name, final String key ) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class JoinRaceMsg extends SendMsg {
    public BotId botId = new BotId();

    public static class BotId {
        public String name;
        public String key;
    }

    public String trackName;
    public String password;
    public int carCount = 1;

    JoinRaceMsg( final String name, final String key, String trackName, int carCount ) {
        this.botId.name = name;
        this.botId.key = key;
        this.trackName = trackName;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class CreateRaceMsg extends SendMsg {
    public BotId botId = new BotId();

    public static class BotId {
        public String name;
        public String key;
    }

    public String trackName;
    public String password;
    public int carCount = 1;

    CreateRaceMsg( final String name, final String key, String trackName, int carCount ) {
        this.botId.name = name;
        this.botId.key = key;
        this.trackName = trackName;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}

class PingMsg extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class SwitchLaneMsg {
    public final String msgType;
    private final String data;

    public SwitchLaneMsg( boolean right ) {
        this.msgType = "switchLane";
        this.data = ( right ) ? "Right" : "Left";
    }

    public String toJson() {
        return new Gson().toJson( this );
    }
}

class ThrottleMsg {
    public final String msgType;
    private final double data;
    private final int gameTick;

    public ThrottleMsg( double throttle, int tick ) {
        this.msgType = "throttle";
        this.data = throttle;
        this.gameTick = tick;
    }

    public String toJson() {
        return new Gson().toJson( this );
    }
}

class TurboMsg {
    public final String msgType = "turbo";
    private final String data = "wroooom!";

    public String toJson() {
        return new Gson().toJson( this );
    }
}

class Utils {

    /**
     * Maps value n from range [a,b] to range [c,d]. Ranges can be in any order (min-max, max-min). Range limits are
     * enforced.
     **/
    public static double mapvalue( double n, double a, double b, double c, double d ) {
        // Check source range
        if ( b == a ) {
            // Stupid to call this way, and we have no way to determine which direction range goes,
            // so let's return always max (the "100%" case)
            return d;
        }

        // Map value
        // (this should work even if the ranges are negative or value goes out of the range)
        n = c + ( ( ( n - a ) / ( b - a ) ) * ( d - c ) );

        // Check that fits inside target range
        if ( d >= c ) {
            n = minmax( c, n, d );
        } else {
            n = minmax( d, n, c );
        }

        // Done
        return n;
    }

    /** Enforces value n between [mina,maxa] **/
    public static double minmax( double mina, double a, double maxa ) {
        return ( a < mina ) ? mina : ( ( a > maxa ) ? maxa : a );
    }

    public static String toStr( double f, int digits, boolean plusSign ) {
        String format = ( digits >= 0 ) ? String.format( "%%s%%.0%df", digits ) : String.format( "%%s%%f" );
        return String.format( format, ( plusSign && ( f >= 0 ) ) ? "+" : "", f );
    }
}
