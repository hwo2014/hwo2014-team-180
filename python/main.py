import sys, socket, json, threading, math, copy, pprint, time

neperc = 2.7182


def minmax(mina, a, maxa):
    if ( a < mina ):
      return mina
    else:
      if ( a > maxa ):
        return maxa
      else:
        return a

def mapvalue(n, a, b, c, d):
    if ( b == a ):
      # Stupid to call this way, and we have no way to determine which direction range goes,
      # so let's return always max (the "100%" case)
      return float(d)

    # Map value
    # (this should work even if the ranges are negative or value goes out of the range)
    n = float(c) + (((float(n) - float(a)) / (float(b) - float(a))) * \
        (float(d) - float(c)))

    # Check that fits inside target range
    if ( d >= c ):
      n = minmax( c, n, d )
    else:
      n = minmax( d, n, c )
    return n

def calc_braking_dist(curr_speed, target_speed):
    if curr_speed == 0:
      return 0
    #-print 'calc_braking_dist', curr_speed, target_speed
    constant = -0.02
    if target_speed == 0:
      target_speed = 0.001
    brake_time = -math.log(float(curr_speed) / target_speed, math.e) / constant
    #-print '    brake_time', brake_time
    integral_1 = curr_speed * (1 / constant) * math.e ** (constant*brake_time)
    integral_2 = curr_speed * (1 / constant) * math.e ** (constant*0)
    #-print '    ', integral_1, integral_2
    distance = integral_1 - integral_2
    #-print 'distance', distance
    return distance

def DEPRECATED_calc_in_curve_throttle(car_drift):
    if car_drift < 10:
      next_thro = 1.0
    elif car_drift < 20:
      next_thro = 0.5
    elif car_drift < 30:
      next_thro = 0.3
    elif car_drift < 40:
      next_thro = 0.2
    elif car_drift < 50:
      next_thro = 0.1
    else:
      next_thro = 0.0
    return next_thro

def get_throttle_to_keep_speed(target_speed):
    return float(target_speed) / 10.0

def DEPRECATED_calc_throttle_to_get_speed(target_speed, car_speed, car_drift):
    stable_throttle = get_throttle_to_keep_speed(target_speed)
    if car_speed > target_speed:
      throttle = mapvalue((car_speed - target_speed),
          0.0, 2.0, stable_throttle, 0.0)
    else:
      throttle = mapvalue((target_speed - car_speed),
          0.0, 2.0, stable_throttle, 1.0)
    return throttle

class CalcEnv(threading.Thread):
  def __init__(self, track, car_pos_hist):
    threading.Thread.__init__(self)
    self.lock = threading.Lock()
    self.track = track
    self.car_pos_hist = car_pos_hist
    self.car_pos_event = self.car_pos_hist.car_pos_event
    self.quit_event = threading.Event()
    self.action_ready = threading.Event()
    self.throttle_val = 1.0
    self.note = ''
    self.state = 0
    self.action = ('throttle', self.throttle_val)
    self.lane_switch_sent = False
    self.last_lane_switch_piece_idx = -1
    self.log = open('log.txt', 'w')

  def need_to_switch_lane(self, pos, next_piece):
    if not 'switch' in next_piece or next_piece['switch'] == False:
      return False
    nxnx_piece = self.track.get_next_piece_of(next_piece)
    next_sw_piece, lefts, rights = self.track.get_data_to_next_switch(nxnx_piece)
    if lefts != rights:
      if rights > lefts:
        return 'Right'
      else:
        return 'Left'
    return False

    '''
    # DEPRECATED
    dist_to_curve_ahead, next_curve_piece = self.track.distance_to_next_curve(
        nxnx_piece, 0)
    curve_to_right = next_curve_piece['angle'] > 0
    piece_after_curve = self.track.get_next_piece_of(next_curve_piece)
    if piece_after_curve['is_curve']:
      curve_to_right = piece_after_curve['angle'] > 0
    if curve_to_right == True:
      return 'Right'
    else:
      return 'Left'
    '''

  def calc_throttle_to_get_speed(self, target_speed, car_speed, car_drift):
    stable_throttle = get_throttle_to_keep_speed(target_speed)
    if car_speed > target_speed:
      self.note += ' ---'
      throttle = mapvalue((car_speed - target_speed),
          0.0, 4.0, stable_throttle, 0.0)
    else:
      self.note += ' +++'
      throttle = mapvalue((target_speed - car_speed),
          0.0, 4.0, stable_throttle, 1.0)
    if abs(car_drift) > 50.0:
      # Never exceed stable value when drift angle is high
      throttle = min(throttle, stable_throttle)
    return throttle

  def calc_target_speed_for_curve(self, mode, curve_piece, lane_idx,
      curve_angle_remaining):
    self.note += '[%.2f] ' % curve_angle_remaining
    # a = (v**2)/radius
    # it seems acceleration a is about 0.5, because
    # 6.5**2/(100-20) --> car barely stays on track
    max_centric_acc = 0.528  # Keimola
    max_centric_acc = 0.5  # USA
    radius, lane_dist_from_center = self.track.get_lane_radius(
        curve_piece, lane_idx)
    radius_mod = mapvalue(radius, 50, 100, 0.9, 1.0)
    # curve with small radius seems to be harder
    max_speed_in_long_curve = math.sqrt(max_centric_acc * radius * radius_mod)
    if mode == '180_curve':
      return max_speed_in_long_curve

    else: # mode == 'partial_curve':
      # long_curve is >= 90 degrees
      # TODO
      max_speed_in_low_curve = max_speed_in_long_curve * 1.3
      max_speed = mapvalue(curve_angle_remaining, 22, 90,
          max_speed_in_low_curve, max_speed_in_long_curve)
      return max_speed

  def optimal_drift_angle(self, curve_angle_travelled, curve_dist_remaining):
    # map any curve < 180 to range [0, 180]
    if curve_angle_travelled + curve_dist_remaining <= 180.0:
      adjusted_trav_ang = mapvalue(curve_angle_travelled,
          0.0, curve_angle_travelled + curve_dist_remaining,
          0.0, 180.0)
    else:
      adjusted_trav_ang = curve_angle_travelled
    #adjusted_trav_ang = curve_angle_travelled # HACK
    if curve_angle_travelled < 25.0:
      optimal_drift = 2.0
    elif adjusted_trav_ang <= 180.0:
      # It was measured 52.0 or 53.0 drift angle in 24 tics --> 2.18 in one tick
      # car travelled 180 degrees in 28 ticks --> 6.42857 degrees in one tick
      # --> when car travels 6.42857 degrees drift angle may rise 2.18 degrees
      # --> 2.18 * (curve_angle_travelled - 25) / 6.42857
      optimal_drift = 0.339 * (adjusted_trav_ang - 25.0) # TODO adjust constant
    else: # > 180
      optimal_drift = 55.0
    return optimal_drift

  def calc_target_speed(self, pos, car_speed, car_drift, piece, in_piece_dist,
      next_piece, lane_idx):
    if piece['is_curve']:
      curve_dist_remaining, curve_angle_remaining = self.track.curve_dist_(
          'remaining', piece, in_piece_dist, lane_idx)
      curve_dist_travelled, curve_angle_travelled = self.track.curve_dist_(
          'travelled', piece, in_piece_dist, lane_idx)

      optimal_speed = self.calc_target_speed_for_curve('partial_curve',
          piece, lane_idx, curve_angle_travelled + curve_angle_remaining) # TODO

      if 0 and (next_piece['is_curve'] == False and
          curve_angle_remaining < 25 and
          car_drift < 50.0):
        # Curve but next is straight => accelerate out of the curve
        self.note += ' next is straight'
        target_speed = 10.0
      elif car_speed < optimal_speed:
        #target_speed = mapvalue(car_drift, 10, 60, 10, 0)
        target_speed = optimal_speed
        self.note += ' < optimal (%.2f)' % optimal_speed
      else: # car_speed >= optimal_speed:
        # curve_angle_remaining == 180 --> optimal speed cannot be exceeded
        target_speed = optimal_speed
        self.note += ' > optimal (%.2f)' % optimal_speed

      car_drift_mod = abs(car_drift)
      optimal_drift = self.optimal_drift_angle(curve_angle_travelled,
          curve_dist_remaining)
      if (piece['angle'] < 0 and car_drift > 0 or
          piece['angle'] > 0 and car_drift < 0):
          # car is drifting on wrong side. we are in trouble
          car_drift_mod = 4*abs(car_drift)
      self.note += ' *%.2f*' % optimal_drift
      drift_diff = abs(optimal_drift - car_drift_mod) / 60.0
      speed_diff = (optimal_speed - car_speed) / 10.0
      # speed_diff == -1 when going 2*optimal_speed
      # speed_diff == 0.5 when going 0.5*optimal_speed

      if car_drift_mod > optimal_drift:
        # for example car drifts 10% decrease speed 10%
        #target_speed -= target_speed * drift_diff * (1-speed_diff)
        self.note += 'DRIFT- (%.2f)' % target_speed
      else:
        #target_speed += target_speed * drift_diff
        self.note += 'DRIFT+ (%.2f)' % target_speed
        
    return target_speed

  def run(self):
    while self.quit_event.is_set() == False:
      self.car_pos_event.wait(6.0)
      self.car_pos_event.clear()
      pos = self.car_pos_hist.get_last_pos()
      if pos == None:
        self.quit_event.wait(0.05)
        continue
      self.calc_env(pos)
      #self.quit_event.wait(0.05)

  def calc_env(self, pos):
    piece_idx = pos['piecePosition']['pieceIndex']
    in_piece_dist = pos['piecePosition']['inPieceDistance']
    end_lane_idx = pos['piecePosition']['lane']['endLaneIndex']
    piece = self.track.get_piece(piece_idx)
    next_piece = self.track.get_piece(piece_idx + 1)
    car_speed = self.car_pos_hist.get_car_speed()
    speed_magnitude = min(1.0, car_speed / 10.0)
    track_angle = piece.get('angle', 0.0)
    track_radius = piece.get('radius', 0.0)
    car_drift = pos.get('angle', 0.0)

    self.note = ''

    if (self.lane_switch_sent == True and
        self.car_pos_hist.is_lane_switch_done()):
      # Previous lane switch request has been sent and processed
      self.lane_switch_sent = False

    #if self.lane_switch_sent == False:
    if self.last_lane_switch_piece_idx != piece_idx:
      switch_dir = self.need_to_switch_lane(pos, next_piece)
      self.last_lane_switch_piece_idx = -1
      if switch_dir != False:
        self.lock.acquire()
        self.action = ('switch_lane', switch_dir)
        self.lock.release()
        print '##################################### switch lane'
        self.lane_switch_sent = True
        self.last_lane_switch_piece_idx = piece_idx
        self.action_ready.set()
        return

    dist_to_curve_ahead, next_curve_piece = self.track.distance_to_next_curve(
        piece_idx, in_piece_dist)

    curve_dist_total, curve_angle_total = self.track.curve_dist_(
        'remaining', next_curve_piece, 0, end_lane_idx)

    curve_target_speed = self.calc_target_speed_for_curve('partial_curve',
        next_curve_piece, end_lane_idx, curve_angle_total)

    braking_dist = calc_braking_dist(car_speed, curve_target_speed)
    # drift_lose_dist: maybe with speed 10 it takes 200 distance to lose drift
    drift_lose_dist = car_drift / 60.0 * 200 * (car_speed / 10.0) # TODO
    braking_dist = max(braking_dist, drift_lose_dist)

    reaction_dist = car_speed * 3
    if piece['is_curve'] == False:
      if dist_to_curve_ahead <= reaction_dist + braking_dist * 1.0:
        # brake before curve
        self.note += 'braking before curve'
        next_thro = 0.0
      elif car_drift >= 54:
        self.note = 'braking on straight to get car straight, huh'
        next_thro = 0.0
      else:
        # straight road ahead
        self.note += 'straight road ahead'
        next_thro = 1.0

    else:
      # inside curve
      #next_thro = calc_in_curve_throttle(car_drift)
      self.note += 'curve'
      target_speed = self.calc_target_speed(pos, car_speed, car_drift,
          piece, in_piece_dist, next_piece, end_lane_idx)
      next_thro = self.calc_throttle_to_get_speed(target_speed, car_speed, car_drift)
      
      '''
      drift_magnitude = min(1.0, float(abs(car_drift)) / 50)
      if drift_magnitude < 0.1 and speed_magnitude < 0.5:
        next_thro = 1.0
      else:
        next_thro = max(0.1,
            self.throttle_val - self.throttle_val * drift_magnitude)
      '''
    if pos['game_tick'] % 1 == 0:
      logmsg = '[%04d] s=%02d d=%03d pi=%03d i=%02d th=%.2f ang=%02d rad=%03d %s' % \
        (pos['game_tick'], int(car_speed*10), int(car_drift*10), int(piece_idx),
         int(in_piece_dist), next_thro, int(track_angle), int(track_radius),
         self.note)
      print logmsg
    self.log.write(logmsg + '\n')
    next_thro = min(1.0, max(0.0, next_thro))
    self.car_pos_hist.store_throttle(next_thro)
    self.car_pos_hist.store_track_info(track_angle, track_radius)

    self.lock.acquire()
    self.throttle_val = next_thro
    #-self.throttle_val = 0.65 # HACK
    self.action = ('throttle', self.throttle_val)
    self.lock.release()
    self.action_ready.set()

  def quit(self):
    self.car_pos_event.set()
    self.quit_event.set()
    self.car_pos_event.set()

  def get_next_action(self):
    self.lock.acquire()
    action = copy.deepcopy(self.action)
    '''
    pos = self.car_pos_hist.get_last_pos()
    if pos == None: pos = {}
    #-print 'speed', self.car_pos_hist.car_speed, pos.get('angle', 0)
    if self.state == 1 or self.car_pos_hist.car_speed >= 4.0:
      action = ('throttle', 0.0)
      self.state = 1
    else:
      action = ('throttle', 1.0)
    if action[0] == 'throttle':
      action = ('throttle', 0.45957)
    '''
    self.lock.release()
    return action

  def wait_next_action(self):
    self.action_ready.wait(0.01)
    if not self.action_ready.is_set():
      print 'Warning! action not ready'
    return self.get_next_action()

class CarPosHistory:
  def __init__(self, track):
    self.positions = []
    self.track = track
    self.lock = threading.Lock()
    self.car_speed = 0.0
    self.car_speeds = []
    self.car_pos_event = threading.Event()
    self.lane_switch_in_progress = False
    self.lane_switch_finished = threading.Event()

  def is_lane_switch_done(self):
    self.lock.acquire()
    res = self.lane_switch_finished.is_set()
    self.lane_switch_finished.clear()
    self.lock.release()
    return res

  def store_track_info(self, angle, radius):
    self.lock.acquire()
    self.positions[-1]['track_angle'] = angle
    self.positions[-1]['track_radius'] = radius
    self.lock.release()

  def store_throttle(self, throttle):
    self.lock.acquire()
    self.positions[-1]['throttle'] = throttle
    self.lock.release()

  def store_pos(self, new_pos, game_tick):
    if game_tick == None:
      game_tick = 0 # maybe race ended
    new_pos['game_tick'] = game_tick
    self.lock.acquire()
    self.positions.append(new_pos)
    lane = new_pos['piecePosition']['lane']
    if lane['startLaneIndex'] != lane['endLaneIndex']:
      self.lane_switch_in_progress = True
    else:
      if self.lane_switch_in_progress == True:
        self.lane_switch_finished.set()
        self.lane_switch_in_progress = False
    self.lock.release()

    piece_idx = new_pos['piecePosition']['pieceIndex']
    in_piece_dist = new_pos['piecePosition']['inPieceDistance']
    self.calc_car_speed(piece_idx, in_piece_dist)
    self.car_pos_event.set()

  def get_last_pos(self, idx=0):
    self.lock.acquire()
    list_idx = -1 + idx
    if list_idx >= 0:
      abs_list_idx = abs(list_idx)
    else:
      abs_list_idx = abs(list_idx) - 1
    if (len(self.positions) > 0 and
        (abs_list_idx < len(self.positions))):
      pos = self.positions[list_idx]
    else:
      pos = None
    self.lock.release()
    return pos

  def get_car_speed(self):
    return self.car_speed

  def write_car_speeds_to_arch(self):
    fobj = open('car_speeds.json', 'w')
    data = {'pos': self.positions, 'speed': self.car_speeds}
    json.dump(data, fobj)
    fobj.close()

  def calc_car_speed(self, piece_idx_0=None, in_piece_dist_0=None):
    if piece_idx_0 == None or in_piece_dist_0 == None:
      pos_0 = self.get_last_pos()
      if pos_0 == None:
        return 0
      piece_idx_0 = pos_0['piecePosition']['pieceIndex']
      in_piece_dist_0 = pos_0['piecePosition']['inPieceDistance']

    pos_1 = self.get_last_pos(-1)
    if pos_1 == None:
      self.car_speed = in_piece_dist_0
    else:
      piece_idx_1 = pos_1['piecePosition']['pieceIndex']
      in_piece_dist_1 = pos_1['piecePosition']['inPieceDistance']
      if piece_idx_0 != piece_idx_1:
        # piece has changed
        last_piece = self.track.get_piece(piece_idx_1)
        dist = last_piece.get('length', 100) # TODO curves
        in_piece_dist_0 += dist
        # TODO: fix curve behavior. Now dont modify self.car_speed
      else:
        self.car_speed = in_piece_dist_0 - in_piece_dist_1
    self.car_speeds.append(self.car_speed)
    return self.car_speed

class Track:
  def __init__(self):
    self.pieces = []
    self.lock = threading.Lock()

  def add_pieces(self, pieces):
    self.lock.acquire()
    self.pieces = pieces
    idx = 0
    for piece in self.pieces:
      piece['index'] = idx
      if 'angle' in piece:
        piece['is_curve'] = True
      else:
        piece['is_curve'] = False
      idx += 1
    #-pprint.pprint(self.pieces)
    self.lock.release()

  def add_lanes(self, lanes):
    # lanes is a list of these: {"distanceFromCenter": -20, "index": 0}
    self.lanes = lanes
    print 'lanes', lanes

  def get_lane_dist_from_center(self, lane_idx):
    distance = 0
    try:
      for lane in self.lanes:
        if lane_idx == lane['index']:
          distance = lane['distanceFromCenter']
    except Exception, ex:
      print 'Warning! something is wrong: %s' % str(ex)
    return distance

  def get_data_to_next_switch(self, piece_id):
    if isinstance(piece_id, int):
      piece_idx = piece_id
      piece = self.get_piece(piece_idx)
    else:
      piece = piece_id
      piece_idx = piece['index']

    lefts = 0
    rights = 0
    while not 'switch' in piece or piece['switch'] == False:
      if piece['is_curve'] == True:
        if piece['angle'] < 0:
          lefts += 1
        elif piece['angle'] > 0:
          rights += 1
      piece = self.get_next_piece_of(piece)
      piece_idx = piece['index']
    return piece, lefts, rights

  def get_next_piece_of(self, piece_idx):
    if isinstance(piece_idx, int):
      next_piece_idx = piece_idx + 1
    else:
      next_piece_idx = piece_idx['index'] + 1
    return self.get_piece(next_piece_idx)

  def get_piece(self, idx):
    # TODO: lock
    if idx >= len(self.pieces):
      idx = idx % len(self.pieces)
    return self.pieces[idx]

  def get_lane_radius(self, piece, lane_idx):
    lane_dist = self.get_lane_dist_from_center(lane_idx)
    if piece['angle'] > 0:
      # TOOD: test
      lane_dist = -lane_dist
    lane_dist = -20 # HACK
    lane_radius = piece['radius'] + lane_dist
    return lane_radius, lane_dist

  def curve_dist_(self, mode, piece_idx, in_piece_dist, lane_idx):
    if isinstance(piece_idx, int):
      piece = self.get_piece(piece_idx)
    else:
      piece = piece_idx
      piece_idx = piece['index']
    if piece['is_curve'] == False:
      return 0

    initial_angle = piece['angle']
    initial_radius = piece['radius']
    modifier = 1.0
    if 0:
      radius, lane_dist = self.get_lane_radius(piece, lane_idx)
    else:
      radius = piece['radius']

    piece_len = 2 * math.pi * radius * float(abs(piece['angle'])) / 360.0
    if mode == 'remaining':
      distance = piece_len - in_piece_dist
      piece_idx += 1
    else:
      distance = in_piece_dist
      piece_idx -= 1
    angle_distance = float(abs(initial_angle)) * distance / float(piece_len)

    piece = self.get_piece(piece_idx)
    # TODO: not perfect
    while (piece['is_curve'] and
        ((initial_angle > 0 and piece['angle'] > 0) or
         (initial_angle < 0 and piece['angle'] < 0))):
      if 0:
        lane_radius = piece['radius'] + lane_dist
      else:
        radius = piece['radius']
      if initial_radius != piece['radius']:
        # for example rad 50 changes to 100 rad --> modifier 0.5
        modifier = float(piece['radius']) / initial_radius

      piece_len = 2 * math.pi * radius * float(abs(piece['angle'])) / 360.0
      distance += piece_len
      angle_distance += abs(piece['angle']) * modifier
      if mode == 'remaining':
        piece_idx += 1
      else:
        piece_idx -= 1
      piece = self.get_piece(piece_idx)
    return abs(distance), abs(angle_distance)

  def distance_to_next_curve(self, curr_piece_idx, curr_in_piece_dist):
    if isinstance(curr_piece_idx, int):
      curr_piece = self.get_piece(curr_piece_idx)
    else:
      curr_piece = curr_piece_idx
      curr_piece_idx = curr_piece['index']
    curve_found = False
    piece = self.get_piece(curr_piece_idx)
    if 'angle' in curr_piece:
      return 0, curr_piece # this is a curve

    distance = curr_piece['length'] - curr_in_piece_dist
    piece_idx = curr_piece_idx + 1
    piece = self.get_piece(piece_idx)
    while not 'angle' in piece:
      distance += piece['length']
      piece_idx += 1
      piece = self.get_piece(piece_idx)
      if piece_idx - curr_piece_idx > len(self.pieces):
        print 'distance_to_next_curve: something is wrong, we looped entire track'
        break
    return distance, piece

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.track = Track()
        self.car_pos_hist = CarPosHistory(self.track)
        self.calculator = CalcEnv(self.track, self.car_pos_hist)
        self.throttle_counter = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def create_race(self, track_id=None):
        data = {
            "botId": {
                "name": self.name,
                "key": self.key,
            },
        }
        if track_id != None or str(track_id) != 0:
          track_name = {'1': 'germany', '2': 'usa'}.get(str(track_id), None)
          data['trackName'] = track_name
        data['password'] = 'cuplii'
        data['carCount'] = 1
        return self.msg("createRace", data)

    def join(self, track_id=None):
        join_data = {
            "name": self.name,
            "key": self.key,
        }
        if track_id != None or str(track_id) != 0:
          track_name = {'1': 'germany', '2': 'usa'}.get(str(track_id), None)
          join_data['trackName'] = track_name
        return self.msg("join", join_data)

    def throttle(self, throttle):
        self.msg("throttle", throttle)
        #if self.throttle_counter % 50 == 0:
        #  print('throttle %s' % str(throttle))
        self.throttle_counter += 1

    def switch_lane(self, direction):
        self.msg("switchLane", direction)

    def ping(self):
        self.msg("ping", {})

    def run(self, track_id=None):
        #self.calculator.start() # HACK
        self.create_race(track_id)
        #self.join(track_id)
        self.msg_loop()
        self.car_pos_hist.write_car_speeds_to_arch()
        self.calculator.quit()

    def on_join(self, data, tick=None):
        print("Joined")
        self.ping()

    def on_game_init(self, data, tick=None):
        print ("Game initialized. Id: %s, Name: %s" % 
            (data['race']['track']['id'], data['race']['track']['name']))
        self.track.add_pieces(data['race']['track']['pieces'])
        self.track.add_lanes(data['race']['track']['lanes'])
        # TODO: store initial pos from "startingPoint"

    def on_game_start(self, data, tick=None):
        print("Race started. Go go go")
        self.ping()

    def on_car_positions(self, data, tick=None):
        for car in data:
          if (isinstance(car, dict) and \
              car.get('id', {}).get('name', '') == self.name):
            self.car_pos_hist.store_pos(car, tick)
            break
        self.calculator.calc_env(self.car_pos_hist.get_last_pos())
        action = self.calculator.get_next_action()
        if tick == None:
          tick = -99
        if action[0] == 'throttle':
          self.throttle(action[1])
        if action[0] == 'switch_lane':
          self.switch_lane(action[1])
        else:
          self.ping()

    def on_crash(self, data, tick=None):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data, tick=None):
        print("Race ended")
        self.ping()

    def on_error(self, data, tick=None):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        try:
          socket_file = s.makefile()
          line = socket_file.readline()
          while line:
              msg = json.loads(line)
              msg_type, data = msg['msgType'], msg['data']
              if msg_type in msg_map:
                if 'gameTick' in msg:
                  msg_map[msg_type](data, msg['gameTick'])
                else:
                  msg_map[msg_type](data)
              else:
                print("Got {0}".format(msg_type))
                self.ping()
              line = socket_file.readline()
        except KeyboardInterrupt, kbint:
          print 'Stopped by CTRL+C'


def test1():
    calc_braking_dist(6.0, 5.0)
    calc_braking_dist(6.0, 4.0)
    calc_braking_dist(6.0, 3.0)
    calc_braking_dist(6.0, 2.0)
    calc_braking_dist(6.0, 1.0)
    calc_braking_dist(6.0, 0.0)

def test2():
    print mapvalue( -53.43, 20, 50, 7, 5 )

def test3():
    print calc_target_speed_for_curve({'radius': 100})
    print calc_target_speed_for_curve({'radius': 200})

def test4():
    print mapvalue(20.0, 10, 60, 10, 0)

def test5():
    print get_throttle_to_keep_speed(6.2)
    print calc_throttle_to_get_speed(6.2, 6.2, 0.0)
    print calc_throttle_to_get_speed(6.2, 6.4, 0.0)
    print calc_throttle_to_get_speed(6.2, 6.0, 0.0)

if __name__ == "__main__":
    #test5()
    #sys.exit(0)
    if not len(sys.argv) in (5, 6):
        print("Usage: ./run host port botname botkey")
    else:
        if len(sys.argv) == 5:
          host, port, name, key = sys.argv[1:5]
          track_id = None
        elif len(sys.argv) == 6:
          host, port, name, key, track_id = sys.argv[1:6]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run(track_id)

'''
    private double calcBrakingDist(double curr_speed, double target_speed) {
        // Call this method on straight road to calculate distance that is
        // required to slow down (brake) before a curve, where we need to
        // have target_speed. curr_speed is current car speed.
        final double neperc = 2.7182;
        final double constant = -0.02;
        double brake_time;
        double integral_1;
        double integral_2;
        double distance;
        if (curr_speed == 0) {
          return 0;
        }
        if (target_speed == 0) {
          target_speed = 0.001;
        }
        
        brake_time = -Math.log(curr_speed / target_speed) / constant;
        integral_1 = curr_speed * (1 / constant) * Math.pow(Math.E, (constant*brake_time));
        integral_2 = curr_speed * (1 / constant) * Math.pow(Math.E, (constant*0));
        distance = integral_1 - integral_2;
        return distance;
    }
'''

